#include "mainwindow.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QSurfaceFormat>


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QSurfaceFormat fmt;
	fmt.setDepthBufferSize(24);
	fmt.setSamples(4);
	//fmt.setVersion(3, 1);
	fmt.setProfile(QSurfaceFormat::CoreProfile);
	QSurfaceFormat::setDefaultFormat(fmt);

	MainWindow mainWindow;
	mainWindow.resize(mainWindow.sizeHint());
	const int desktopArea = QApplication::desktop()->width() *
	                        QApplication::desktop()->height();
	const int widgetArea = mainWindow.width() * mainWindow.height();
	if ((static_cast<float>(widgetArea) / static_cast<float>(desktopArea)) < 0.75f)
		mainWindow.show();
	else
		mainWindow.showMaximized();
	return app.exec();
}
