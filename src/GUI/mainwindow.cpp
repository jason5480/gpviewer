#include "mainwindow.h"
#include "glwidget.h"

#include <QMenu>
#include <QFileDialog>
#include <QTimer>
#include <QDebug>
#include <QProgressBar>

MainWindow::MainWindow()
	: mAnimOn(false)
	, mTimeWindowframe(0)
	, mLastFrame(1000)
{
	setupUi(this);

	openGLWidget->setColorMode(GLWidget::COLORMODE::HSV);
	openGLWidget->setSelectMode(GLWidget::SELECTMODE::VERTEX);

	mDumpdeformer = new DumpDeformer(openGLWidget);
	mCiftideformer = new CIFTIDeformer(openGLWidget);

	// Connect signals with deformers custom stots
	connect(checkBox, &QCheckBox::toggled, mDumpdeformer, &DumpDeformer::onCheckBoxToggled);
	connect(timebarSlider, &QSlider::valueChanged, mCiftideformer, &CIFTIDeformer::onSliderValueChanged);

	// Connect standard check boxes
	connect(openGLWidget, &GLWidget::wireframeModeChanged, wireframeCheckBox, &QCheckBox::setCheckState);
	connect(wireframeCheckBox, &QCheckBox::stateChanged, openGLWidget, &GLWidget::setWireframeMode);

	connect(openGLWidget, &GLWidget::axesModeChanged, axesCheckBox, &QCheckBox::toggle);
	connect(axesCheckBox, &QCheckBox::toggled, openGLWidget, &GLWidget::setAxesMode);

	connect(openGLWidget, &GLWidget::bboxModeChanged, bboxCheckBox, &QCheckBox::toggle);
	connect(bboxCheckBox, &QCheckBox::toggled, openGLWidget, &GLWidget::setBboxMode);

	mTimer.setInterval(200. / speedSpinBox->value());

	// Connect timers' tick signal
	connect(speedSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &MainWindow::setInterval);

	// Create progressBar
	progressBar = new QProgressBar;
	progressBar->setMaximumHeight(16);
	progressBar->setMaximumWidth(200);
	progressBar->setTextVisible(false);
	progressBar->setValue(0);
	statusBar()->setVisible(true);
	progressBar->setVisible(true);
	statusBar()->showMessage("Loading...");
	statusBar()->addPermanentWidget(progressBar, 0);
	statusBar()->setVisible(false);
	progressBar->setVisible(false);

	// connect openGLWidget with progress bar
	connect(openGLWidget, &GLWidget::progress, progressBar, &QProgressBar::setValue);
	connect(openGLWidget, &GLWidget::aboutToLoad, this, &MainWindow::showProgressBar);
	connect(openGLWidget, &GLWidget::modelLoaded, this, &MainWindow::hideProgressBar);
	connect(openGLWidget, &GLWidget::aboutToSave, this, &MainWindow::showProgressBar);
	connect(openGLWidget, &GLWidget::modelSaved, this, &MainWindow::hideProgressBar);

	// connect actions
	connect(actionOpenFile, &QAction::triggered, openGLWidget, static_cast<void (GLWidget::*)()>(&GLWidget::load));
	connect(actionSave, &QAction::triggered, openGLWidget, &GLWidget::save);
	connect(actionScreenShot, &QAction::triggered, openGLWidget, &GLWidget::screenshot);
	connect(actionHelp, &QAction::triggered, this, &MainWindow::help);

	// connect buttons
	connect(playPushButton, &QPushButton::clicked, this, &MainWindow::onPlayClicked);
	connect(rewindPushButton, &QPushButton::clicked, this, &MainWindow::onRewindClicked);
	connect(cameraPushButton, &QPushButton::clicked, openGLWidget, &GLWidget::calibrateCamera);

	// connect time bar slider
	connect(timebarSlider, &QSlider::valueChanged, this, &MainWindow::onTimebarChanged);
}

MainWindow::~MainWindow()
{
	disconnect(checkBox, &QCheckBox::toggled, mDumpdeformer, &DumpDeformer::onCheckBoxToggled);
	disconnect(timebarSlider, &QSlider::valueChanged, mCiftideformer, &CIFTIDeformer::onSliderValueChanged);
}

void MainWindow::help()
{
	qDebug() << "Help Here";
}

void MainWindow::animateForward()
{
	// Stop animation or repeat depending on checkbox value
	if (mTimeWindowframe == mLastFrame)
	{
		mTimeWindowframe = 0;
		// Stop
		if (!repeateCheckBox->isChecked())
		{
			mAnimOn = false;
			timebarSlider->setValue(timebarSlider->maximum());
			playPushButton->setText("Play");
			playPushButton->setIcon(QIcon(":/onPlayClicked.png"));
			timebarSlider->setValue(mTimeWindowframe);
			disconnect(&mTimer, &QTimer::timeout, this, &MainWindow::animateForward);
			mTimer.stop();

			// Call update so that changes will be rendered
			openGLWidget->update();
			return;
		}
	}

	// Update timebar
	timebarSlider->setValue(mTimeWindowframe);

	mTimeWindowframe++;
}

void MainWindow::animateBack()
{
	// Stop animation or repeat depending on checkbox value
	if (mTimeWindowframe == 0)
	{
		mTimeWindowframe = mLastFrame;
		// Stop
		if (!repeateCheckBox->isChecked())
		{
			mAnimOn = false;
			timebarSlider->setValue(0);
			rewindPushButton->setText("Rw");
			rewindPushButton->setIcon(QIcon(":/rewind.png"));
			timebarSlider->setValue(mTimeWindowframe);
			mTimer.stop();

			// Call update so that changes will be rendered
			openGLWidget->update();
			return;
		}
	}

	// Update timebar
	timebarSlider->setValue(mTimeWindowframe);

	mTimeWindowframe--;
}

void MainWindow::onPlayClicked()
{
	if (mAnimOn)
	{
		playPushButton->setText("Play");
		playPushButton->setIcon(QIcon(":/onPlayClicked.png"));
		mAnimOn = false;
		mTimer.stop();
		rewindPushButton->setText("Rw");
		rewindPushButton->setIcon(QIcon(":/rewind.png"));
	}
	else
	{
		disconnect(&mTimer, &QTimer::timeout, this, &MainWindow::animateBack);
		connect(&mTimer, &QTimer::timeout, this, &MainWindow::animateForward);
		playPushButton->setText("Pause");
		playPushButton->setIcon(QIcon(":/pause.png"));
		mAnimOn = true;
		mTimer.start();
	}
}

void MainWindow::onRewindClicked()
{
	if (mAnimOn)
	{
		rewindPushButton->setText("Rw");
		rewindPushButton->setIcon(QIcon(":/rewind.png"));
		mAnimOn = false;
		mTimer.stop();
		playPushButton->setText("Play");
		playPushButton->setIcon(QIcon(":/onPlayClicked.png"));
	}
	else
	{
		disconnect(&mTimer, &QTimer::timeout, this, &MainWindow::animateForward);
		connect(&mTimer, &QTimer::timeout, this, &MainWindow::animateBack);
		rewindPushButton->setText("Pause");
		rewindPushButton->setIcon(QIcon(":/pause.png"));
		mAnimOn = true;
		mTimer.start();
	}
}

void MainWindow::onTimebarChanged()
{
	mTimeWindowframe = timebarSlider->value();
}

void MainWindow::setInterval(double speed)
{
	mTimer.setInterval(200./speed);
	qDebug() << "Interval is:" << 200. / speed << "msec";
}

void MainWindow::showProgressBar(const QString& label)
{
	progressBar->setValue(0);
	statusBar()->setVisible(true);
	progressBar->setVisible(true);
	statusBar()->showMessage(label);
	statusBar()->messageChanged(label);
} 

void MainWindow::hideProgressBar()
{
	statusBar()->clearMessage();
	statusBar()->setVisible(false);
	progressBar->setVisible(false);
	progressBar->setValue(0);
}
