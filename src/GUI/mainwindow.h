#pragma once

#include "ui_mainwindow.h"
#include "ciftideformer.h"
#include "dumpdeformer.h"

#include <QMainWindow>
#include <QTimer>

class CIFTIDeformer;
class DumpDeformer;
class QProgressBar;

/*******************************************************************************
  MainWindow
  ******************************************************************************/

class MainWindow : public QMainWindow, private Ui::MainWindow
{
Q_OBJECT

public:
    // ctor
    MainWindow();
    ~MainWindow();

private:
    CIFTIDeformer* mCiftideformer;
    DumpDeformer* mDumpdeformer;
    QTimer mTimer;

    bool mAnimOn;
    int mTimeWindowframe;
    int mLastFrame;

    QProgressBar *progressBar;

private slots:
    void help();
    void onPlayClicked();
    void onRewindClicked();
    void onTimebarChanged();
    void animateForward();
    void animateBack();
    void setInterval(double speed);
    void showProgressBar(const QString& label);
    void hideProgressBar();
};
