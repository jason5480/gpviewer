#include "ciftideformer.h"
#include <QtMath>

/*******************************************************************************
Implementation of MeshDeformer methods
******************************************************************************/

CIFTIDeformer::CIFTIDeformer(GLWidget* wid) : MeshDeformer(wid)
{
	connect(openGLWidget, &GLWidget::modelLoaded, this, &CIFTIDeformer::onModelLoaded);
	connect(openGLWidget, &GLWidget::vertexSelected, this, &CIFTIDeformer::onVertexSelected);
	connect(openGLWidget, &GLWidget::trianglesSelected, this, &CIFTIDeformer::onTrianglesSelected);
}

CIFTIDeformer::~CIFTIDeformer()
{
	disconnect(openGLWidget, &GLWidget::modelLoaded, this, &CIFTIDeformer::onModelLoaded);
	disconnect(openGLWidget, &GLWidget::vertexSelected, this, &CIFTIDeformer::onVertexSelected);
	disconnect(openGLWidget, &GLWidget::trianglesSelected, this, &CIFTIDeformer::onTrianglesSelected);
}

void CIFTIDeformer::onModelLoaded()
{
	init(openGLWidget->vertices(), openGLWidget->normals(), openGLWidget->colors(), openGLWidget->indices());
	QVector3D size = openGLWidget->size();
	setMax(size);
}

void CIFTIDeformer::onVertexSelected(int vertexID)
{
	populateColorsPerVertex(vertexID);
	openGLWidget->setColorMode(GLWidget::COLORMODE::HSV);
	openGLWidget->setColors(mColors);
}

void CIFTIDeformer::onTrianglesSelected(QList<int> triangleIDs)
{
	populateColorsPerTriangle(triangleIDs);
	openGLWidget->setColorMode(GLWidget::COLORMODE::HSV);
	openGLWidget->setColors(mColors);
}

void CIFTIDeformer::init(const QVector<QVector3D>& vertices, const QVector<QVector3D>& normals, const QVector<QVector3D>& colors, const QVector<uint>& indices)
{
	mVertices = vertices;
	mNormals = normals;
	mColors = colors;
	mIndices = indices;
}

void CIFTIDeformer::process()
{
	populateColorsWithBar();
}

void CIFTIDeformer::setBarValue(int val)
{
	mBarValue = val;
}

void CIFTIDeformer::setMax(QVector3D& max) {
	mMax = qFabs(max.x());
	if (qFabs(max.y()) > mMax) mMax = qFabs(max.y());
	if (qFabs(max.z()) > mMax) mMax = qFabs(max.z());
}

void CIFTIDeformer::populateColorsWithBar()
{
	if (!mColors.empty()) mColors.clear();

	// Give color based on the y coordinate in modelspace
	for (int i = 0; i < mVertices.count(); i++)
	{
		mColors.append(QVector3D((mVertices[i].y() + mBarValue + 100.) / 200., 1, 0.5));
	}
}

void CIFTIDeformer::populateColorsPerVertex(int targetPointID)
{
	if (!mColors.empty()) mColors.clear();

	//qDebug() << "Colors per vertex populating.." << m_colors.count();

	// Give color based on y value in modelspace
	for (int i = 0; i < mVertices.count(); i++)
	{
		mColors.append(QVector3D(mVertices[targetPointID].distanceToPoint(mVertices[i]) / mMax, 1, 0.5));
	}
	mColors[targetPointID].setY(0);
	mColors[targetPointID].setZ(0);
	//qDebug() << "Colors populated sucessfully" << m_colors.count();
}

void CIFTIDeformer::populateColorsPerTriangle(QList<int> triangleIDs)
{
	//qDebug() << "Colors per vertex populating.." << m_colors.count();

	// Give color based on y value in modelspace
	for (int i = 0; i < triangleIDs.count(); i++)
	{
		mColors[mIndices[3 * triangleIDs[i]]].setY(0);
		mColors[mIndices[3 * triangleIDs[i]]].setZ(0);

		mColors[mIndices[3 * triangleIDs[i] + 1]].setY(0);
		mColors[mIndices[3 * triangleIDs[i] + 1]].setZ(0);

		mColors[mIndices[3 * triangleIDs[i] + 2]].setY(0);
		mColors[mIndices[3 * triangleIDs[i] + 2]].setZ(0);
	}
	//qDebug() << "Colors populated sucessfully" << m_colors.count();
}

void CIFTIDeformer::setRandomCorrelations()
{
	if (mCorrelations.count()) mCorrelations.clear();
	mCorrelations.reserve(mVertices.count());

	for (int i = mVertices.count(); i > 0; i--)
	{
		QVector<float> temp(i);
		for (int j = 0; j < i; j++)
		{
			temp[j] = mVertices[i - 1].distanceToPoint(mVertices[j]) / 100.;
		}
		mCorrelations.prepend(temp);
	}
}

void CIFTIDeformer::onSliderValueChanged(int val)
{
	setBarValue(val);
	populateColorsWithBar();
	openGLWidget->setColorMode(GLWidget::COLORMODE::HSV);
	openGLWidget->setColors(mColors);
}
