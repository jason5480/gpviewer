#pragma once

#include "ciftideformerdll.h"

#include "meshdeformer.h"

/*******************************************************************************
  MeshDeformer
  ******************************************************************************/

class CiftiDeformer_API CIFTIDeformer : public MeshDeformer
{
Q_OBJECT

public:
	// Ctor
	explicit CIFTIDeformer(GLWidget* wid);
	~CIFTIDeformer();

private:
	// Methods
	void init(const QVector<QVector3D>& vertices, const QVector<QVector3D>& normals, const QVector<QVector3D>& colors, const QVector<uint>& indices);
	void process();
	void setBarValue(int val);
	void setMax(QVector3D& max);
	void populateColorsWithBar();
	void populateColorsPerVertex(int targetPointID);
	void populateColorsPerTriangle(QList<int> triangleIDs);
	void setRandomCorrelations();

private:
	// Data members
	QVector<QVector3D> mVertices;
	QVector<QVector3D> mNormals;
	QVector<QVector3D> mColors;
	QVector<uint> mIndices;
	QList< QVector<float> > mCorrelations;
	int mBarValue;
	qreal mMax;

public slots:
	void onModelLoaded() Q_DECL_OVERRIDE;
	void onVertexSelected(int vertexID) Q_DECL_OVERRIDE;
	void onTrianglesSelected(QList<int> triangleIDs) Q_DECL_OVERRIDE;
	void onSliderValueChanged(int val);
};