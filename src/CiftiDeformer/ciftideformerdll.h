#ifdef WIN32
#   ifdef CiftiDeformer_EXPORTS
#       define CiftiDeformer_API __declspec(dllexport)
#   else
#       define CiftiDeformer_API  __declspec(dllimport)
#   endif
#else
#   define CiftiDeformer_API
#endif // WIN32