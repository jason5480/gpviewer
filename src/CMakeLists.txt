# Main
cmake_minimum_required(VERSION 2.8.11)
project(GPViewer)

### QT STUFF #############################################################################
if (DEFINED ENV{QTDIR})
  set(CMAKE_PREFIX_PATH $ENV{QTDIR})
else()
  set(CMAKE_PREFIX_PATH CACHE PATH "Location of Qt")
  message(${CMAKE_PREFIX_PATH})
  message("QTDIR system variable not found! add CMAKE_PREFIX_PATH path manually")
endif()
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# The rest should just work for Qt5 sure!
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)

# for grouping into folders
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

### Build subdirs. Also add them to include path #########################################
add_subdirectory(GUI)
add_subdirectory(GLWidget)
add_subdirectory(DumpDeformer)
add_subdirectory(CiftiDeformer)
##########################################################################################