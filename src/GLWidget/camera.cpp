#include "camera.h"
#include <QVector3D>
#include <QQuaternion>
#include <QMatrix4x4>
#include <QtMath>
#include <QDebug>

/*******************************************************************************
  Implementation of Camera methods
  ******************************************************************************/

Camera::Camera()
	: QObject()
	, mFov(45)
	, mAspect(1)
	, mZNear(1)
	, mZFar(500)
	, mEye(0, 0, 300)
	, mFocal(0, 0, 0)
	, mUpvec(0, 1, 0)
	, mCDist(300)
	, mZoomStep(5)
	, mSensitivity(1)
{
}

const QVector3D& Camera::eye()
{
	return mEye;
}

const QVector3D& Camera::focal() const
{
	return mFocal;
}

const QVector3D& Camera::upvector() const
{
	return mUpvec;
}

const QVector3D& Camera::translation() const
{
	return mTranslation;
}

const QMatrix4x4& Camera::projectionMatrix() const
{
	return mProjectionMatrix;
}

const QMatrix4x4& Camera::viewMatrix() const
{
	return mViewMatrix;
}

qreal Camera::distance() const
{
	return mCDist;
}

qreal Camera::sensitivity() const
{
	return mSensitivity;
}

void Camera::translate(QVector3D& trans)
{
	mTranslation += mSensitivity * trans;
	updateView();
}

void Camera::rotate(QQuaternion& rot)
{
	mRotation = rot * mRotation;
	updateView();
}

void Camera::calibrate(BBox& box)
{
	mFocal = box.center();
	(qFabs(box.max().x()) > qFabs(box.max().y())) ?
		mCDist = qFabs(box.max().x()) / qTan(qDegreesToRadians(mFov / 2)) + qFabs(box.max().z()) :
		mCDist = qFabs(box.max().y()) / qTan(qDegreesToRadians(mFov / 2)) + qFabs(box.max().z());
	mEye = QVector3D(0, 0, mCDist);
	// Set sensitivity
	mSensitivity = mCDist / 100;
	// Reset rotation
	mRotation = QQuaternion();
	// Reset traslation
	mTranslation = QVector3D();
	// Recalculate near and far plane
	mZNear = mSensitivity;
	mZFar = 500 * mSensitivity;
	updateView();
	updateProjection();
}

void Camera::setAspect(qreal aspect)
{
	mAspect = aspect;
	updateProjection();
}

void Camera::setDistance(int wheel)
{
	mCDist -= mZoomStep * mSensitivity * (wheel / 120.);
	if (mCDist < 15 * mSensitivity) mCDist = 15 * mSensitivity;
	if (mCDist > 450 * mSensitivity) mCDist = 450 * mSensitivity;
	mEye.setZ(mCDist);
	updateView();
}

void Camera::updateView()
{
	mViewMatrix.setToIdentity();
	// Translate the whole system
	mViewMatrix.translate(mTranslation);
	// Look at the (0,0,0) point
	mViewMatrix.lookAt(mEye, QVector3D(0,0,0), mUpvec);
	// Rotate based on quaternion
	mViewMatrix.rotate(mRotation);
	// Move to the center of the model
	mViewMatrix.translate(-mFocal);
	emit updated();
}

void Camera::updateProjection()
{
	// Reset projection
	mProjectionMatrix.setToIdentity();
	// Set perspective projection
	mProjectionMatrix.perspective(mFov, mAspect, mZNear, mZFar);
	emit updated();
}