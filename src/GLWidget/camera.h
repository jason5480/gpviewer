#pragma once

#include "bbox.h"
#include <QVector3D>
#include <QMatrix4x4>

class Camera : public QObject
{
	Q_OBJECT

public:
	// Ctor
	Camera();
	~Camera() = default;

	// Methods
	const QVector3D& eye();
	const QVector3D& focal() const;
	const QVector3D& upvector() const;
	const QVector3D& translation() const;
	const QMatrix4x4& projectionMatrix() const;
	const QMatrix4x4& viewMatrix() const;
	qreal distance() const;
	qreal sensitivity() const;

	void translate(QVector3D& trans);
	void rotate(QQuaternion& rot);
	void calibrate(BBox& box);
	void setAspect(qreal aspect);
	void setDistance(int wheel);

private:
	// Data members
	// Perspective
	qreal mFov;
	qreal mAspect;
	qreal mZNear;
	qreal mZFar;
	// View
	QVector3D mEye;
	QVector3D mFocal;
	QVector3D mUpvec;
	// Transforamtions
	QVector3D mTranslation;
	QQuaternion mRotation;
	// Matrices
	QMatrix4x4 mProjectionMatrix;
	QMatrix4x4 mViewMatrix;
	// Interaction
	qreal mCDist;
	qreal mZoomStep;
	qreal mSensitivity;

	// Update the corresponding Matrices
	void updateView();
	void updateProjection();

signals:
	void updated();
};