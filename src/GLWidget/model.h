#pragma once

#include "ray.h"
#include "bbox.h"

#include <QVector3D>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>

/*******************************************************************************
  Model
  ******************************************************************************/

class Model : public QObject, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	enum VIEW
	{
		FRONT = 1,
		BACK = 1 << 1,
		RIGHT = 1 << 2,
		LEFT = 1 << 3,
		UP = 1 << 4,
		DOWN = 1 << 5
	};

	// Ctor
	Model();
	Model(QOpenGLShaderProgram& program);
	Model(QString& modelFilename, QOpenGLShaderProgram& program, VIEW type = FRONT);
	~Model();

	// Methods
	void init(QOpenGLShaderProgram& program, VIEW type = FRONT);
	void loadNew(const QString& modelFilename);
	void save(const QString& modelFilename);
	QVector<QVector3D>& vertices();
	QVector<QVector3D>& normals();
	QVector<QVector3D>& colors();
	QVector<GLuint>& indices();
	void setVertices(const QVector<QVector3D>& vertices);
	void setColors(const QVector<QVector3D>& colors);
	void recalculateNormals();
	BBox& bbox();
	void draw(QOpenGLShaderProgram* program, int wireframe = 0, bool bbox = false, bool axes = true);

	void setWorldMatrix(VIEW type);
	QMatrix4x4& worldMatrix();
	void intersectWithRay(Ray& r, int& closest, QList<int>& triList);

private:
	// Methods
	void load(const QString& modelFilename);
	void loadOBJ();
	void loadOFF();
	void saveOBJ(const QString& objFilename);
	void saveOFF(const QString& offFilename);
	void calculateNormals();
	void reorderNormals(QVector<GLuint>& normalIndices, QVector<QVector3D>& normals);
	void calculateBbox();
	void calculateAxes();
	bool intersectTriWithRay(Ray& r, int triNum, float& tau, int& closest);
	void setupVertexAttribs();
	void setupBboxVertexAttribs();
	void setupAxesVertexAttribs();
	void updateVertices();
	void updateNormals();
	void updateColors();
	void updateBbox();
	void updateAll();

	// Data members
	QString mModelFilename;
	QVector<QVector3D> mVertices;
	QVector<QVector3D> mNormals;
	QVector<QVector3D> mColors;
	QVector<GLuint> mIndices;

	BBox m_bbox;
	QVector<QVector3D> mBboxdata;
	QVector<GLuint> mBboxindices;

	QVector<QVector3D> mAxesdata;
	QVector<GLuint> mAxesindices;

	// Transformation matrix that converts local coordinates to world coordinates
	QMatrix4x4 mWorldMatrix;

	// OpenGL stuff
	QOpenGLVertexArrayObject mVao;
	QOpenGLBuffer mVertexBuf;
	QOpenGLBuffer mNormalBuf;
	QOpenGLBuffer mColorBuf;
	QOpenGLBuffer mIndexBuf;

	QOpenGLVertexArrayObject mBboxvao;
	QOpenGLBuffer mBboxBuf;
	QOpenGLBuffer mBboxindexBuf;

	QOpenGLVertexArrayObject mAxesvao;
	QOpenGLBuffer mAxesBuf;
	QOpenGLBuffer mAxesindexBuf;

public slots:
	void cleanup();

signals:
	void verticesChanged();
	void normalsChanged();
	void colorsChanged();
	void modelChanged();
	void progress(int val);
};
