#include "glwidget.h"
#include "ray.h"

#include <QtWidgets>
#include <QPixmap>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>

GLWidget::GLWidget(QWidget *parent)
	: QOpenGLWidget(parent)
	, wireframe(0)
	, bbox(false)
	, axes(false)
	, mLastClosestPointId(0)
	, mTransparent(true)
	, mColorMode(COLORMODE::RGB)
	, mSelectMode(SELECTMODE::NONE)

{
	setFocusPolicy(Qt::WheelFocus);
	setAcceptDrops(true);

	if (mTransparent) setAttribute(Qt::WA_TranslucentBackground);

	connect(&mCamera, &Camera::updated, this, static_cast<void (GLWidget::*)()>(&GLWidget::update));
}

GLWidget::~GLWidget()
{
	cleanup();
}

void GLWidget::cleanup()
{
	// Make sure the context is current when deleting the buffers.
	makeCurrent();
	// No pointers to delete
	doneCurrent();
}

const QVector<QVector3D>& GLWidget::vertices()
{
	return mModel.vertices();
}

const QVector<QVector3D>& GLWidget::normals()
{
	return mModel.normals();
}

const QVector<QVector3D>& GLWidget::colors()
{
	return mModel.colors();
}

const QVector<GLuint>& GLWidget::indices()
{
	return mModel.indices();
}

QVector3D GLWidget::size()
{
	return 2 * mModel.bbox().max();
}

void GLWidget::setVertices(const QVector<QVector3D>& vertices)
{
	mModel.setVertices(vertices);
}

void GLWidget::setColors(const QVector<QVector3D>& colors)
{
	mModel.setColors(colors);
}

void GLWidget::calculateNormals()
{
	mModel.recalculateNormals();
}

void GLWidget::setColorMode(COLORMODE colorMode)
{
	mColorMode = colorMode;
}

void GLWidget::setSelectMode(SELECTMODE selectMode)
{
	mSelectMode = selectMode;
}

void GLWidget::load(const QString& filename)
{
	if (!filename.isEmpty())
	{
		QStringList tok = filename.split("/", QString::SkipEmptyParts);
		emit aboutToLoad(tok[tok.size() - 1] + " is loading...");
		mModel.loadNew(filename);
		emit modelLoaded();
		// Our camera changes in this example.
		mCamera.calibrate(mModel.bbox());
	}
}

void GLWidget::load()
{
	QString formatOBJ = "obj";
	QString formatOFF = "off";
	const QString initialPath = QDir::currentPath() + tr("/untitled.") + formatOBJ;

	const QString fileName = QFileDialog::getOpenFileName(this, tr("Load Model"), initialPath,
		tr("%1 Files (*.%2);;%3 Files (*.%4);;All Files (*)")
		.arg(formatOBJ.toUpper()).arg(formatOBJ).arg(formatOFF.toUpper()).arg(formatOFF));

	load(fileName);
}

void GLWidget::save()
{
	QString formatOBJ = "obj";
	QString formatOFF = "off";
	const QString initialPath = QDir::currentPath() + tr("/untitled.") + formatOBJ;

	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Model"), initialPath,
		tr("%1 Files (*.%2);;%3 Files (*.%4);;All Files (*)")
		.arg(formatOBJ.toUpper()).arg(formatOBJ).arg(formatOFF.toUpper()).arg(formatOFF));

	if (!fileName.isEmpty())
	{
		// Save the model
		QStringList tok = fileName.split("/", QString::SkipEmptyParts);
		emit aboutToSave("Saving " + tok[tok.size() - 1] + "...");
		mModel.save(fileName);
		emit modelSaved();
	}
}

void GLWidget::screenshot()
{
	qDebug() << "Save screenshot...";
	QScreen *screen = QGuiApplication::primaryScreen();
	if (screen)
	{
		// Capture
		qApp->beep();

		QPoint topLeft = mapToGlobal(rect().topLeft());
		QSize windowSize = rect().size();
		QPixmap pix = screen->grabWindow(0, topLeft.x(), topLeft.y(), windowSize.width(), windowSize.height());
		if (mTransparent) pix.setMask(pix.createMaskFromColor(Qt::black));

		// Save
		QString format = "png";
		const QString initialPath = QDir::currentPath() + tr("/untitled.") + format;

		QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), initialPath,
			tr("%1 Files (*.%2);;All Files (*)")
			.arg(format.toUpper())
			.arg(format));
		if (!fileName.isEmpty() && pix.save(fileName, format.toLatin1().constData()))
		{
			qDebug() << "Screenshot saved";
		}
	}
}

void GLWidget::calibrateCamera()
{

	mCamera.calibrate(mModel.bbox());
}

QSize GLWidget::minimumSizeHint() const
{
	return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
	return QSize(800, 600);
}

void GLWidget::initializeGL()
{
	// In this example the widget's corresponding top-level window can change
	// several times during the widget's lifetime. Whenever this happens, the
	// QOpenGLWidget's associated context is destroyed and a new one is created.
	// Therefore we have to be prepared to clean up the resources on the
	// aboutToBeDestroyed() signal, instead of the destructor. The emission of
	// the signal will be followed by an invocation of initializeGL() where we
	// can recreate all resources.
	connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

	initializeOpenGLFunctions();

	// White backround
	glClearColor(0, 0, 0, mTransparent ? 0 : 1);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	glLineWidth(1);

	initShaders();

	initModel(Model::VIEW::FRONT);

	// Our camera changes in this example.
	mCamera.calibrate(mModel.bbox());

	//GLfloat dist = mCamera.distance();

	// Bind the program
	mProgram.bind();

	// Light position is fixed.
	mProgram.setUniformValue("lightPosition_cameraspace", QVector3D(0, 0, 0));
	mProgram.setUniformValue("lightColor", QVector3D(1, 1, 1));
	//m_program.setUniformValue("lightPower", dist * dist);
	mProgram.setUniformValue("isWireframe", 0);
	mProgram.setUniformValue("isHSV", static_cast<GLfloat>(mColorMode));
	mProgram.release();
	// Use QBasicTimer because its faster than QTimer
	//m_timer.start(12, this);
}

void GLWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Get the current world Matrix transformation from model
	const QMatrix4x4 worldMatrix = mModel.worldMatrix();
	// Get the current view Matrix transformation from camera
	const QMatrix4x4 viewMatrix = mCamera.viewMatrix();
	// Calculate modelview Matrix
	mMvMatrix = viewMatrix * worldMatrix;

	mProgram.bind();
	mProgram.setUniformValue("projMatrix", mCamera.projectionMatrix());
	mProgram.setUniformValue("mvMatrix", mMvMatrix);
	mProgram.setUniformValue("viewMatrix", viewMatrix);
	mProgram.setUniformValue("modelMatrix", worldMatrix);
	mProgram.setUniformValue("normalMatrix", mMvMatrix.normalMatrix());
	mProgram.setUniformValue("isHSV", static_cast<GLfloat>(mColorMode));

	mModel.draw(&mProgram, wireframe, bbox, axes);

	mProgram.release();
}

void GLWidget::resizeGL(int width, int height)
{
	// Calculate aspect ratio
	const qreal aspect = qreal(width) / qreal(height ? height : 1);
	// Set perspective projection
	mCamera.setAspect(aspect);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::RightButton) && (mSelectMode & (SELECTMODE::VERTEX | SELECTMODE::TRIANGLES)))
	{
		Ray ray_modelspace;
		castRay(event->pos(), ray_modelspace);

		// Get a list with intersecting triangles
		int closestPointID = mLastClosestPointId;
		QList<int> triainter;
		mModel.intersectWithRay(ray_modelspace, closestPointID, triainter);

		if (!triainter.empty())
		{
			if ((mSelectMode & SELECTMODE::VERTEX) && (closestPointID != mLastClosestPointId))
			{
				emit vertexSelected(closestPointID);
				mLastClosestPointId = closestPointID;
			}
			if (mSelectMode & SELECTMODE::TRIANGLES) emit trianglesSelected(triainter);
		}
		//else qDebug() << "No selected point/triangles found";
	}

	mLastPos = QVector2D(event->localPos());
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
	// Mouse release position - mouse press position
	QVector2D diff = QVector2D(event->localPos()) - mLastPos;

	if ((event->buttons() & Qt::LeftButton))
	{
		// Rotation axis is perpendicular to the mouse position difference vector
		QVector3D rotationAxis(diff.y(), diff.x(), 0.0);
		// Accelerate angular speed relative to the length of the mouse sweep
		const qreal angle = diff.length() / 10.0;

		// Update rotation
		QQuaternion rot = QQuaternion::fromAxisAndAngle(rotationAxis.normalized(), angle);
		mCamera.rotate(rot);
	}
	if (event->buttons() & Qt::RightButton && (mSelectMode & (SELECTMODE::VERTEX | SELECTMODE::TRIANGLES)))
	{
		Ray rayModelspace;
		castRay(event->pos(), rayModelspace);

		// Get a list with intersecting triangles
		int closestPointID;
		QList<int> triainter;
		mModel.intersectWithRay(rayModelspace, closestPointID, triainter);

		if (!triainter.empty())
		{
			if ((mSelectMode & SELECTMODE::VERTEX) && (closestPointID != mLastClosestPointId))
			{
				emit vertexSelected(closestPointID);
				mLastClosestPointId = closestPointID;
			}
			if (mSelectMode & SELECTMODE::TRIANGLES) emit trianglesSelected(triainter);
		}
		//else qDebug() << "No selected point/triangles found";
	}
	mLastPos = QVector2D(event->localPos());
}

/*
void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
	// Mouse release position - mouse press position
	QVector2D diff = QVector2D(event->localPos()) - m_lastPos;

	// Rotation axis is perpendicular to the mouse position difference
	// vector
	QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

	// Accelerate angular speed relative to the length of the mouse sweep
	qreal acc = diff.length() / 100.0;

	// Calculate new rotation axis as weighted sum
	m_rotationAxis = (m_rotationAxis * m_angularSpeed + n * acc).normalized();

	// Increase angular speed
	m_angularSpeed += acc;
}
//*/

void GLWidget::wheelEvent(QWheelEvent *event)
{
	mCamera.setDistance(event->delta());
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
	//int modif = mkModif(event);
	QString c = event->text();
	const unsigned char key = c.toLatin1()[0];
	switch (isprint(key) ? tolower(key) : key)
	{
	case 'p':
		screenshot();
		break;
	case 'w':
		{
			wireframe = ++wireframe % 3;
			emit wireframeModeChanged(static_cast<Qt::CheckState>(wireframe));
			update();
		}
		break;
	case 'b':
		{
			bbox = !bbox;
			emit bboxModeChanged(bbox);
			update();
		}
		break;
	case 'a':
		{
			axes = !axes;
			emit axesModeChanged(axes);
			update();
		}
		break;
	case 'c':
		mCamera.calibrate(mModel.bbox());
		break;
	case 'l':
		load();
		break;
	case 's':
		save();
		break;
	case '?':
		qDebug() << "Only latin1 characters are supported";
		break;
	default:
		break;
	}

	QVector3D tran;
	switch (event->key())
	{
	case Qt::Key_Up:
		tran.setY(1);
		break;
	case Qt::Key_Down:
		tran.setY(-1);
		break;
	case Qt::Key_Right:
		tran.setX(1);
		break;
	case Qt::Key_Left:
		tran.setX(-1);
		break;
	case Qt::Key_Escape:
		qApp->quit();
		break;
	default: 
		break;
	}
	mCamera.translate(tran);
}

void GLWidget::keyReleaseEvent(QKeyEvent *event)
{}

int GLWidget::mkModif(QInputEvent *event)
{
	const int ctrl = event->modifiers() & Qt::ControlModifier ? 1 : 0;
	const int shift = event->modifiers() & Qt::ShiftModifier ? 1 : 0;
	const int alt = event->modifiers() & Qt::AltModifier ? 1 : 0;
	const int modif = (ctrl << 0) | (shift << 1) | (alt << 2);
	return modif;
}

/*
void GLWidget::timerEvent(QTimerEvent *)
{
	// Decrease angular speed (friction)
	m_angularSpeed *= 0.99;

	// Stop rotation when speed goes below threshold
	if (m_angularSpeed < 0.01) {
		m_angularSpeed = 0.0;
	}
	else {
		// Update rotation
		m_rotation = QQuaternion::fromAxisAndAngle(m_rotationAxis, m_angle) * m_rotation;

		// Request an update
		update();
	}
}
//*/

void GLWidget::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasUrls()) {
		event->acceptProposedAction();
	}
}

void GLWidget::dropEvent(QDropEvent *event)
{
	/*const bool ctrl_down = event->keyboardModifiers() & Qt::ControlModifier ? true : false;
	const bool shift_down = event->keyboardModifiers() & Qt::ShiftModifier ? true : false;
	const bool alt_down = event->keyboardModifiers() & Qt::AltModifier ? true : false;*/

	const QMimeData* mimeData = event->mimeData();

	if (mimeData->hasUrls())
	{
		QList<QUrl> urlList = mimeData->urls();
		//const int num_of_files = urlList.size();

		if (urlList.size() == 1)
		{
			const QString path = urlList[0].toLocalFile();
			load(path);
		}
		else qDebug() << "Cannot proccess more than one files";
	}
}

void GLWidget::initShaders()
{
	if (!mProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vertShader.glsl"))
	{
		const GLenum err = glGetError();
		qDebug() << "OpenGL ERROR No:" << err;
		close();
	}
	if (!mProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fragShader.glsl"))
	{
		const GLenum err = glGetError();
		qDebug() << "OpenGL ERROR No:" << err;
		close();
	}
	if (!mProgram.link())
	{
		const GLenum err = glGetError();
		qDebug() << "OpenGL ERROR No:" << err;
		close();
	}
	if (!mProgram.bind())
	{
		const GLenum err = glGetError();
		qDebug() << "OpenGL ERROR No:" << err;
		close();
	}
	else mProgram.release();
}

void GLWidget::initModel(Model::VIEW type)
{
	// Load the model
	mModel.init(mProgram, type);

	connect(&mModel, &Model::verticesChanged, this, static_cast<void (GLWidget::*)()>(&GLWidget::update));
	connect(&mModel, &Model::normalsChanged, this, static_cast<void (GLWidget::*)()>(&GLWidget::update));
	connect(&mModel, &Model::colorsChanged, this, static_cast<void (GLWidget::*)()>(&GLWidget::update));
	connect(&mModel, &Model::progress, this, &GLWidget::onPregressChanged);
}

void GLWidget::castRay(const QPoint& pos, Ray& r)
{
	const int w = width();
	const int h = height();
	// Constuct ray in camera space

	// Convert selected pixel from screen space to normalized [-1, 1] cliping space
	const QVector4D target_clipingspace(((pos.x() + 0.5) / (0.5 * w)) - 1,
		-(((pos.y() + 0.5) / (0.5 * h)) - 1), 0, 1);

	// Convert target to camera space
	const QVector3D target_cameraspace = (mCamera.projectionMatrix().inverted() * target_clipingspace).toVector3DAffine();

	// Convert ray from camera to model space
	const QVector3D eye_modelspace = mMvMatrix.inverted() * QVector3D(0, 0, 0);
	const QVector3D target_modelspace = mMvMatrix.inverted() * target_cameraspace;

	// Get the ray in model space
	r.set(eye_modelspace, target_modelspace);
}

void GLWidget::setWireframeMode(int state)
{
	wireframe = state;
	update();
}

void GLWidget::setAxesMode(bool state)
{
	axes = state;
	update();
}

void GLWidget::setBboxMode(bool state)
{
	bbox = state;
	update();
}

void GLWidget::onPregressChanged(int val)
{
	emit progress(val);
}
