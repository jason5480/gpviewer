#pragma once

#include <QVector3D>

/*******************************************************************************
  BBox
  ******************************************************************************/

class BBox
{
public:
    // Ctor
    BBox() : mCenter(), mMax(){}
    BBox(const QVector3D center, const QVector3D max) : mCenter(center), mMax(max){}
    ~BBox() = default;

    // Methods
    const QVector3D& center() const { return mCenter; }
    const QVector3D& max() const { return mMax; }
    void set(const QVector3D& center, const QVector3D& max)
    {
        mCenter = center;
        mMax = max;
    }

private:
    // Data members
    QVector3D mCenter;
    QVector3D mMax;
};
