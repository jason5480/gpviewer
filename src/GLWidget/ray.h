#pragma once

#include <QVector3D>

/*******************************************************************************
  Ray
  ******************************************************************************/

class Ray
{
public:
	// Ctor
	Ray() : mStart(), mEnd(), mDir(){}
	Ray(QVector3D& start, QVector3D& end)
		: mStart(start)
		, mEnd(end)
		, mDir((end - start).normalized()){}

	~Ray() = default;

	// Methods
	const QVector3D& start() const { return mStart; }
	const QVector3D& end() const { return mEnd; }
	const QVector3D& dir() const { return mDir; }
	void set(const QVector3D& start, const QVector3D& end)
	{
		mStart = start;
		mEnd = end;
		mDir = (end - start).normalized();
	}

private:
	// Data members
	QVector3D mStart;
	QVector3D mEnd;
	QVector3D mDir;
};