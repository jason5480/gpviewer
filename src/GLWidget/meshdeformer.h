#pragma once

#include "glwidgetdll.h"
#include "glwidget.h"

#include <QObject>
#include <QList>
#include <QVector3D>

/*******************************************************************************
  MeshDeformer
  ******************************************************************************/

class GLWidget_API MeshDeformer : public QObject
{
	Q_OBJECT

public:
	// Ctor
	MeshDeformer();

	explicit MeshDeformer(GLWidget *wid);

	virtual ~MeshDeformer()
	{
	}

	// Methods
	void setWidget(GLWidget *wid)
	{
		openGLWidget = wid;
	}

protected:
	GLWidget *openGLWidget;

public slots:
	virtual void onModelLoaded() = 0;

	virtual void onVertexSelected(int vertexID)
	{
	}

	virtual void onTrianglesSelected(QList<int> triangleIDs)
	{
	}
};

inline MeshDeformer::MeshDeformer() : openGLWidget(nullptr)
{
}

inline MeshDeformer::MeshDeformer(GLWidget *wid)
{
	setWidget(wid);
}
