#pragma once

#include "glwidgetdll.h"

#include "camera.h"
#include "model.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QPoint>
#include <QVector2D>
#include <QVector3D>
#include <QMatrix4x4>
#include <QString>
#include <QBasicTimer>

class QKeyEvent;
class QMouseEvent;
class QWheelEvent;
class QInputEvent;
class QDragEnterEvent;
class QDropEvent;

/*******************************************************************************
  GLWidget
  ******************************************************************************/

class GLWidget_API GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	enum class COLORMODE
	{
		RGB,
		HSV
	};
	enum SELECTMODE
	{
		NONE,
		VERTEX = 1,
		TRIANGLES = 1 << 1
	};

	// ctor
	explicit GLWidget(QWidget* parent = nullptr);
	~GLWidget();

	// Methods
	const QVector<QVector3D>& vertices();
	const QVector<QVector3D>& normals();
	const QVector<QVector3D>& colors();
	const QVector<GLuint>& indices();
	QVector3D size();

	void setVertices(const QVector<QVector3D>& vertices);
	void setColors(const QVector<QVector3D>& colors);
	void calculateNormals();
	void setColorMode(COLORMODE colorMode);
	void setSelectMode(SELECTMODE selectMode);
	void load(const QString& filename);
	void load();
	void save();
	void screenshot();
	void calibrateCamera();
	QSize minimumSizeHint() const Q_DECL_OVERRIDE;
	QSize sizeHint() const Q_DECL_OVERRIDE;

	int wireframe;
	bool bbox;
	bool axes;
	// The Timer
	QBasicTimer timer;

protected:
	// OpenGL framework Methods 
	void initializeGL() Q_DECL_OVERRIDE;
	void paintGL() Q_DECL_OVERRIDE;
	void resizeGL(int width, int height) Q_DECL_OVERRIDE;
	// IO handle Methods
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	//void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	int  mkModif(QInputEvent *event);
	// Timer Event handle
	//void timerEvent(QTimerEvent *e) Q_DECL_OVERRIDE;
	// Drag and drop event
	void dragEnterEvent(QDragEnterEvent *event) override;
	void dropEvent(QDropEvent *event) override;

private:
	// Methods
	void initShaders();
	void initModel(Model::VIEW type = Model::VIEW::FRONT);
	void castRay(const QPoint& pos, Ray& r);

	// Shader program
	QOpenGLShaderProgram mProgram;
	// Camera
	Camera mCamera;
	// Model
	Model mModel;

	// Transformation Matrices
	QMatrix4x4 mMvMatrix;

	// Window Properties
	QVector2D mLastPos;
	int mLastClosestPointId;
	bool mTransparent;
	COLORMODE mColorMode;
	SELECTMODE mSelectMode;

public slots:
	void cleanup();
	void setWireframeMode(int state);
	void setAxesMode(bool state);
	void setBboxMode(bool state);

private slots:
	void onPregressChanged(int val);

signals:
	void wireframeModeChanged(Qt::CheckState state);
	void bboxModeChanged(bool drawBbox);
	void axesModeChanged(bool drawBbox);
	void progress(int val);
	void aboutToLoad(const QString& name);
	void modelLoaded();
	void aboutToSave(const QString& name);
	void modelSaved();
	void vertexSelected(int vertexID);
	void trianglesSelected(QList<int> triangleIDs);
};