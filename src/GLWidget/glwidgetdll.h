#ifdef WIN32
#   ifdef GLWidget_EXPORTS
#       define GLWidget_API __declspec(dllexport)
#   else
#       define GLWidget_API  __declspec(dllimport)
#   endif
#else
#   define GLWidget_API
#endif // WIN32