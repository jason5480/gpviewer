#include "model.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QFile>
#include <QTextStream>
#include <QDebug>

/*******************************************************************************
  Implementation of Model methods
  ******************************************************************************/

Model::Model()
	: QObject(nullptr)
	, mVertexBuf(QOpenGLBuffer::VertexBuffer)
	, mNormalBuf(QOpenGLBuffer::VertexBuffer)
	, mColorBuf(QOpenGLBuffer::VertexBuffer)
	, mIndexBuf(QOpenGLBuffer::IndexBuffer)
	, mBboxBuf(QOpenGLBuffer::VertexBuffer)
	, mBboxindexBuf(QOpenGLBuffer::IndexBuffer)
	, mAxesBuf(QOpenGLBuffer::VertexBuffer)
	, mAxesindexBuf(QOpenGLBuffer::IndexBuffer)
{
}

Model::Model(QOpenGLShaderProgram &program)
	: Model()
{
	init(program);
}

Model::Model(QString &modelFilename, QOpenGLShaderProgram &program, VIEW type)
	: Model()
{
	load(modelFilename);
	init(program, type);
}

Model::~Model()
{
	cleanup();
}

void Model::init(QOpenGLShaderProgram &program, VIEW type)
{
	initializeOpenGLFunctions();

	// Set the world transformation matrix
	setWorldMatrix(type);

	// Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
	// implementations this is optional and support may not be present
	// at all. Nonetheless the below code works in all cases and makes
	// sure there is a VAO when one is needed.
	mVao.create();
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);

	// Generate 4 VBOs for Model
	mVertexBuf.create();
	mNormalBuf.create();
	mColorBuf.create();
	mIndexBuf.create();

	setupVertexAttribs();

	// Tell OpenGL which VBOs to use
	mVertexBuf.bind();
	// Tell OpenGL programmable pipeline how to locate vertex position data
	const int vertexLocation = program.attributeLocation("vertexPosition_modelspace");
	program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
	program.enableAttributeArray(vertexLocation);
	mVertexBuf.release();

	// Tell OpenGL which VBOs to use
	mNormalBuf.bind();
	// Tell OpenGL programmable pipeline how to locate vertex position data
	const int normalLocation = program.attributeLocation("vertexNormal_modelspace");
	program.setAttributeBuffer(normalLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
	program.enableAttributeArray(normalLocation);
	mNormalBuf.release();

	// Tell OpenGL which VBOs to use
	mColorBuf.bind();
	// Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
	const int colorLocation = program.attributeLocation("vertexColor");
	program.setAttributeBuffer(colorLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
	program.enableAttributeArray(colorLocation);
	mColorBuf.release();

	// Generate 2 VBOs for Bbox and Axes
	mBboxvao.create();
	QOpenGLVertexArrayObject::Binder bboxvaoBinder(&mBboxvao);
	mBboxBuf.create();
	mBboxindexBuf.create();

	setupBboxVertexAttribs();

	// Tell OpenGL which VBOs to use
	mBboxBuf.bind();
	// Tell OpenGL programmable pipeline how to locate vertex position data
	program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, 2 * sizeof(QVector3D));
	program.enableAttributeArray(vertexLocation);
	program.disableAttributeArray(normalLocation);
	program.setAttributeBuffer(colorLocation, GL_FLOAT, sizeof(QVector3D), 3, 2 * sizeof(QVector3D));
	program.enableAttributeArray(colorLocation);
	mBboxBuf.release();

	mAxesvao.create();
	QOpenGLVertexArrayObject::Binder axesvaoBinder(&mAxesvao);
	mAxesBuf.create();
	mAxesindexBuf.create();

	setupAxesVertexAttribs();

	// Tell OpenGL which VBOs to use
	mAxesBuf.bind();
	// Tell OpenGL programmable pipeline how to locate vertex position data
	program.setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, 2 * sizeof(QVector3D));
	program.enableAttributeArray(vertexLocation);
	program.disableAttributeArray(normalLocation);
	program.setAttributeBuffer(colorLocation, GL_FLOAT, sizeof(QVector3D), 3, 2 * sizeof(QVector3D));
	program.enableAttributeArray(colorLocation);
	mAxesBuf.release();
}

void Model::cleanup()
{
	// Destroy 4 VBOs
	mVertexBuf.destroy();
	mNormalBuf.destroy();
	mColorBuf.destroy();
	mIndexBuf.destroy();
	// Destroy 2 VBOs
	mBboxBuf.destroy();
	mBboxindexBuf.destroy();
	// Destroy 2 VBOs
	mAxesBuf.destroy();
	mAxesindexBuf.destroy();
}

void Model::loadNew(const QString &modelFilename)
{
	load(modelFilename);
	setupVertexAttribs();
	setupBboxVertexAttribs();
	setupAxesVertexAttribs();
	// No need setupBboxVertexAttribs(); becouse the number of bbox verts are always the same 
	updateAll();
}

void Model::save(const QString &modelFilename)
{
	if (modelFilename.endsWith(".obj", Qt::CaseInsensitive))
		saveOBJ(modelFilename);
	else if (modelFilename.endsWith(".off", Qt::CaseInsensitive))
		saveOFF(modelFilename);
	else qDebug() << "File format not supported";
}

QVector<QVector3D>& Model::vertices()
{
	return mVertices;
}

QVector<QVector3D>& Model::normals()
{
	return mNormals;
}

QVector<QVector3D>& Model::colors()
{
	return mColors;
}

QVector<GLuint>& Model::indices()
{
	return mIndices;
}

void Model::setVertices(const QVector<QVector3D> &vertices)
{
	mVertices = vertices;
	updateVertices();
}

void Model::setColors(const QVector<QVector3D> &colors)
{
	mColors = colors;
	updateColors();
}

void Model::recalculateNormals()
{
	calculateNormals();
	updateNormals();
}

void Model::draw(QOpenGLShaderProgram *program, int wireframe, bool bbox, bool axes)
{
	// Bind the program
	program->bind();

	// Bind the models VAO
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);

	// Tell OpenGL which VBOs to use
	mVertexBuf.bind();
	mNormalBuf.bind();
	mColorBuf.bind();
	mIndexBuf.bind();

	if (wireframe == Qt::Unchecked)
	{
		program->setUniformValue("isWireframe", static_cast<GLfloat>(0.0));
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		// Draw cube geometry using indices from VBO 3
		glDrawElements(GL_TRIANGLES, mIndices.count(), GL_UNSIGNED_INT, nullptr);
	}
	else if (wireframe == Qt::PartiallyChecked)
	{
		program->setUniformValue("isWireframe", static_cast<GLfloat>(0.0));
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		// Draw cube geometry using indices from VBO 3
		glDrawElements(GL_TRIANGLES, mIndices.count(), GL_UNSIGNED_INT, nullptr);
		program->setUniformValue("isWireframe", static_cast<GLfloat>(1.0));
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		// Draw cube geometry using indices from VBO 3
		glDrawElements(GL_TRIANGLES, mIndices.count(), GL_UNSIGNED_INT, nullptr);
	}
	else if (wireframe == Qt::Checked)
	{
		program->setUniformValue("isWireframe", static_cast<GLfloat>(0.0));
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		// Draw cube geometry using indices from VBO 3
		glDrawElements(GL_TRIANGLES, mIndices.count(), GL_UNSIGNED_INT, nullptr);
	}

	// Release the buffers
	mVertexBuf.release();
	mNormalBuf.release();
	mColorBuf.release();
	mIndexBuf.release();

	if (bbox)
	{
		// Bind the Bbox VAO
		QOpenGLVertexArrayObject::Binder bboxVaoBinder(&mBboxvao);

		// Tell OpenGL which VBOs to use
		mBboxBuf.bind();
		mBboxindexBuf.bind();

		glDrawElements(GL_LINES, mBboxindices.count(), GL_UNSIGNED_INT, nullptr);

		mBboxBuf.release();
		mBboxindexBuf.release();
	}
	if (axes)
	{
		// Bind the Bbox VAO
		QOpenGLVertexArrayObject::Binder axesVaoBinder(&mAxesvao);

		// Tell OpenGL which VBOs to use
		mAxesBuf.bind();
		mAxesindexBuf.bind();

		glDrawElements(GL_LINES, mAxesindices.count(), GL_UNSIGNED_INT, nullptr);

		mAxesBuf.release();
		mAxesindexBuf.release();
	}
	// Release the program
	program->release();
}

void Model::load(const QString &modelFilename)
{
	mModelFilename = modelFilename;

	if (mModelFilename.endsWith(".obj", Qt::CaseInsensitive))
		loadOBJ();
	else if (mModelFilename.endsWith(".off", Qt::CaseInsensitive))
		loadOFF();
	else qDebug() << "File format not supported";

	calculateBbox();
	calculateAxes();
}

void Model::loadOBJ()
{
	qDebug() << "Model " << mModelFilename << " is Loading...";
	QFile file(mModelFilename);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		// Clear previous data
		if (mVertices.count())
			mVertices.clear();
		if (mNormals.count())
			mNormals.clear();
		if (mColors.count())
			mColors.clear();
		if (mIndices.count())
			mIndices.clear();

		QTextStream in(&file);
		in.setRealNumberPrecision(6);
		QVector<QVector3D> tempNormals;
		QVector<GLuint> m_normalIndices;

		int numOfVertices = 543524;
		int numOfTriangles = 1087451;
		emit progress(0);

		while (!in.atEnd())
		{
			QString line = in.readLine();
			// If line is empty go to the next one
			if (line.isEmpty())
				continue;
			// Split line in tokens
			QStringList tokens = line.split(" ", QString::SkipEmptyParts);

			if (tokens[0] == "v")
			{
				mVertices.append(QVector3D(tokens[1].toFloat(),
				                           tokens[2].toFloat(),
				                           tokens[3].toFloat()));

				if (tokens.count() == 7)
					mColors.append(QVector3D(tokens[4].toFloat(), tokens[5].toFloat(), tokens[6].toFloat()));
				else
					mColors.append(QVector3D(0.8f, 0.8f, 0.8f));
			}
			else if (tokens[0] == "vn")
			{
				tempNormals.append(QVector3D(tokens[1].toFloat(), tokens[2].toFloat(), tokens[3].toFloat()));
			}
			else if (tokens[0] == "f")
			{
				QStringList tok1 = tokens[1].split("/", QString::KeepEmptyParts);
				QStringList tok2 = tokens[2].split("/", QString::KeepEmptyParts);
				QStringList tok3 = tokens[3].split("/", QString::KeepEmptyParts);

				const int v1 = tok1[0].toInt() - 1;
				const int v2 = tok2[0].toInt() - 1;
				const int v3 = tok3[0].toInt() - 1;

				mIndices.append(v1);
				mIndices.append(v2);
				mIndices.append(v3);

				if (tok1.count() == 3 && tok2.count() == 3 && tok3.count() == 3)
				{
					const int n1 = tok1[2].toInt() - 1;
					const int n2 = tok2[2].toInt() - 1;
					const int n3 = tok3[2].toInt() - 1;

					m_normalIndices.append(n1);
					m_normalIndices.append(n2);
					m_normalIndices.append(n3);
				}
			}
			else if (tokens[0] == "#")
			{
				if (tokens.size() == 3)
				{
					if (tokens[1] == "Vertices:")
					{
						numOfVertices = tokens[2].toInt();
					}
					else if (tokens[1] == "Faces:")
					{
						numOfTriangles = tokens[2].toInt();
					}
				}
			}

			emit progress(66 * (mVertices.count() + mIndices.count()) / (numOfVertices + 3 * numOfTriangles));
		}
		file.close();

		if (tempNormals.count())
		{
			if (mVertices.count() == tempNormals.count())
				mNormals = tempNormals;
			else
				reorderNormals(m_normalIndices, tempNormals);
		}
		else
			calculateNormals();

		emit progress(99);

		qDebug() << "Model " << mModelFilename << " Loaded successfully!!";
		qDebug() << "Vertices: " << mVertices.count() << "Normals:" << mNormals.count() << "Colors:" << mColors.count() << "Triangles:" << mIndices.count() / 3;
	}
	else
	{
		qDebug() << "Unable to open file: " << mModelFilename;
		file.close();
	}
}

void Model::loadOFF()
{
	qDebug() << "Model OFF " << mModelFilename << " is Loading...";
	QFile file(mModelFilename);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		// Clear previous data
		if (mVertices.count())
			mVertices.clear();
		if (mNormals.count())
			mNormals.clear();
		if (mColors.count())
			mColors.clear();
		if (mIndices.count())
			mIndices.clear();

		QTextStream in(&file);
		in.setRealNumberPrecision(6);

		emit progress(0);

		QString firstLine = in.readLine();
		QString secondLine = in.readLine();
		QStringList sizes = secondLine.split(" ", QString::SkipEmptyParts);
		int numOfVertices = 50000;
		int numOfTriangles = 100000;
		if (sizes.size() == 3)
		{
			numOfVertices = sizes[0].toInt();
			numOfTriangles = sizes[1].toInt();
		}

		while (!in.atEnd())
		{
			QString line = in.readLine();
			// If line is empty go to the next one
			if (line.isEmpty())
				continue;
			// Split line in tokens
			QStringList tokens = line.split(" ", QString::SkipEmptyParts);

			// Check if the numbers are integer or floats
			bool areAllInts = true;
			bool isInt = false;
			for (int i = 0; i < tokens.count(); i++)
			{
				tokens[i].toInt(&isInt);
				areAllInts = areAllInts & isInt;
			}
			if (!areAllInts)
			{
				mVertices.append(QVector3D(tokens[0].toFloat(),
				                           tokens[1].toFloat(),
				                           tokens[2].toFloat()));
				if (tokens.count() == 7)
					mColors.append(QVector3D(tokens[3].toFloat() / 255, tokens[4].toFloat() / 255, tokens[5].toFloat() / 255));
				else
					mColors.append(QVector3D(0.8f, 0.8f, 0.8f));
			}
			else if (areAllInts)
			{
				const int numOfVertPerFace = tokens[0].toInt();
				if (numOfVertPerFace == 3)
				{
					mIndices.append(tokens[1].toInt());
					mIndices.append(tokens[2].toInt());
					mIndices.append(tokens[3].toInt());
				}
				else qDebug() << "Quads are not supported in line:" << line;
			}
			emit progress(66 * (mVertices.count() + mIndices.count()) / (numOfVertices + 3 * numOfTriangles));
		}
		file.close();

		calculateNormals();

		emit progress(99);
		qDebug() << "Model " << mModelFilename << " Loaded successfully!!";
		qDebug() << "Vertices: " << mVertices.count() << "Normals:" << mNormals.count() << "Colors:" << mColors.count() << "Triangles:" << mIndices.count() / 3;
	}
	else
	{
		qDebug() << "Unable to open file: " << mModelFilename;
		file.close();
	}
}

void Model::saveOBJ(const QString &objFilename)
{
	qDebug() << "Exporting OBJ...";

	QFile file(objFilename);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream out(&file);
		out.setRealNumberNotation(QTextStream::FixedNotation);
		out.setRealNumberPrecision(6);
		out << "# VVR General purpose viewer" << endl;
		out << "# " + (objFilename) << endl;
		out << "# Vertices: " << mVertices.count() << endl;
		out << "# Normals: " << mNormals.count() << endl;
		out << "# Triangles: " << mIndices.count() / 3 << endl;
		for (int i = 0; i < mVertices.count(); i++)
		{
			out << "v" << " " << mVertices[i].x()
				<< " " << mVertices[i].y()
				<< " " << mVertices[i].z() << endl;

			emit progress(33 * i / mVertices.count());
		}
		for (int i = 0; i < mNormals.count(); i++)
		{
			out << "vn" << " " << mNormals[i].x()
				<< " " << mNormals[i].y()
				<< " " << mNormals[i].z() << endl;

			emit progress(33 + 33 * i / mNormals.count());
		}
		out << "s 1" << endl;
		for (int i = 0; i < mIndices.count(); i += 3)
		{
			out << "f" << " " << mIndices[i] + 1 << "//" << mIndices[i] + 1
				<< " " << mIndices[i + 1] + 1 << "//" << mIndices[i + 1] + 1
				<< " " << mIndices[i + 2] + 1 << "//" << mIndices[i + 2] + 1 << endl;

			emit progress(66 + 33 * i / mIndices.count());
		}
		file.close();
	}

	qDebug() << "OBJ Export Complete";
}

void Model::saveOFF(const QString &offFilename)
{
	qDebug() << "Exporting OFF...";

	QFile file(offFilename);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream out(&file);
		out << "OFF" << endl;
		out << mVertices.count() << " " << mIndices.count() / 3 << " 0" << endl;
		for (int i = 0; i < mVertices.count(); i++)
		{
			out << mVertices[i].x()
				<< " " << mVertices[i].y()
				<< " " << mVertices[i].z() << endl;

			emit progress(50 * i / mVertices.count());
		}
		for (int i = 0; i < mIndices.count(); i += 3)
		{
			out << "3" << " " << mIndices[i]
				<< " " << mIndices[i + 1]
				<< " " << mIndices[i + 2] << endl;

			emit progress(50 + 50 * i / mIndices.count());
		}
		file.close();
	}

	qDebug() << "OFF Export Complete";
}

void Model::calculateNormals()
{
	// Clear pervious data
	if (mNormals.count())
		mNormals.clear();
	// Allocate the memory and initialize it with zeroes
	mNormals.fill(QVector3D(), mVertices.count());
	qDebug() << "Calculating Normals...";

	for (int i = 0; i < mIndices.count() / 3; i++)
	{
		// Non normalized triangle normal
		const QVector3D norm = QVector3D::crossProduct((mVertices[mIndices[3 * i + 1]] - mVertices[mIndices[3 * i]]),
		                                               (mVertices[mIndices[3 * i + 2]] - mVertices[mIndices[3 * i]]));
		mNormals[mIndices[3 * i]] += norm;
		mNormals[mIndices[3 * i + 1]] += norm;
		mNormals[mIndices[3 * i + 2]] += norm;

		emit progress(66 + 999 * i / mIndices.count());
	}
	// Normalize per vertex normals
	for (int i = 0; i < mNormals.count(); i++)
	{
		mNormals[i].normalize();
	}
	qDebug() << "Normals Calculated sucessfully" << mNormals.count();
}

void Model::reorderNormals(QVector<GLuint> &normalIndices, QVector<QVector3D> &normals)
{
	mNormals.resize(mVertices.count());
	qDebug() << "Reordering Normals...";
	for (int i = 0; i < mIndices.count(); i++)
	{
		mNormals[mIndices[i]] = normals[normalIndices[i]];

		emit progress(66 + 33 * i / mIndices.count());
	}
}

void Model::calculateBbox()
{
	QVector3D max;
	QVector3D min;

	if (mVertices.count())
		min = mVertices[0];

	for (auto vertex : mVertices)
	{
		if (max.x() < vertex.x())
			max.setX(vertex.x());
		if (max.y() < vertex.y())
			max.setY(vertex.y());
		if (max.z() < vertex.z())
			max.setZ(vertex.z());
		if (min.x() > vertex.x())
			min.setX(vertex.x());
		if (min.y() > vertex.y())
			min.setY(vertex.y());
		if (min.z() > vertex.z())
			min.setZ(vertex.z());
	}
	const QVector3D cen((max + min) / 2);
	QVector3D rad((max - min) / 2);
	// Convert to world space
	m_bbox.set(mWorldMatrix * cen, mWorldMatrix * rad);

	// Clear previous data
	if (mBboxdata.count())
		mBboxdata.clear();
	if (mBboxindices.count())
		mBboxindices.clear();

	// Set up the vertex data
	// 0
	mBboxdata.append(cen - rad);
	mBboxdata.append(QVector3D(1, 0, 1));
	// 1
	mBboxdata.append(cen + QVector3D(rad.x(), -rad.y(), -rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	// 2
	mBboxdata.append(cen + QVector3D(rad.x(), -rad.y(), rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	// 3
	mBboxdata.append(cen + QVector3D(-rad.x(), -rad.y(), rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	// 4
	mBboxdata.append(cen + QVector3D(-rad.x(), rad.y(), -rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	// 5
	mBboxdata.append(cen + QVector3D(rad.x(), rad.y(), -rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	// 6
	mBboxdata.append(cen + rad);
	mBboxdata.append(QVector3D(1, 0, 1));
	// 7
	mBboxdata.append(cen + QVector3D(-rad.x(), rad.y(), rad.z()));
	mBboxdata.append(QVector3D(1, 0, 1));
	//*/
	// Set up the indices
	// 0-1
	mBboxindices.append(0);
	mBboxindices.append(1);
	// 1-2
	mBboxindices.append(1);
	mBboxindices.append(2);
	// 2-3
	mBboxindices.append(2);
	mBboxindices.append(3);
	// 3-0
	mBboxindices.append(3);
	mBboxindices.append(0);
	// 4-5
	mBboxindices.append(4);
	mBboxindices.append(5);
	// 5-6
	mBboxindices.append(5);
	mBboxindices.append(6);
	// 6-7
	mBboxindices.append(6);
	mBboxindices.append(7);
	// 7-4
	mBboxindices.append(7);
	mBboxindices.append(4);
	// 0-4
	mBboxindices.append(0);
	mBboxindices.append(4);
	// 1-5
	mBboxindices.append(1);
	mBboxindices.append(5);
	// 2-6
	mBboxindices.append(2);
	mBboxindices.append(6);
	// 3-7
	mBboxindices.append(3);
	mBboxindices.append(7);
}

void Model::calculateAxes()
{
	// Clear previous data
	if (mAxesdata.count())
		mAxesdata.clear();
	if (mAxesindices.count())
		mAxesindices.clear();

	const QVector3D cen = mWorldMatrix.inverted() * m_bbox.center();
	const float xlength = (mWorldMatrix.inverted() * m_bbox.max()).x() * 1.2;
	const float ylength = (mWorldMatrix.inverted() * m_bbox.max()).y() * 1.2;
	const float zlength = (mWorldMatrix.inverted() * m_bbox.max()).z() * 1.2;

	// Set up the vertex data
	// 0
	mAxesdata.append(cen);
	mAxesdata.append(QVector3D(0, 1, 1));
	// +X
	mAxesdata.append(cen + QVector3D(xlength, 0, 0));
	mAxesdata.append(QVector3D(0, 1, 1));
	// 0
	mAxesdata.append(cen);
	mAxesdata.append(QVector3D(0.33f, 1, 1));
	// +Y
	mAxesdata.append(cen + QVector3D(0, ylength, 0));
	mAxesdata.append(QVector3D(0.33f, 1, 1));
	// 0
	mAxesdata.append(cen);
	mAxesdata.append(QVector3D(0.66f, 1, 1));
	// +Z
	mAxesdata.append(cen + QVector3D(0, 0, zlength));
	mAxesdata.append(QVector3D(0.66f, 1, 1));

	// Set up the indices
	// X
	mAxesindices.append(0);
	mAxesindices.append(1);
	// Y
	mAxesindices.append(2);
	mAxesindices.append(3);
	// Z
	mAxesindices.append(4);
	mAxesindices.append(5);
}

void Model::setWorldMatrix(VIEW type)
{
	// Set the world transformation matrix
	mWorldMatrix.setToIdentity();
	switch (type)
	{
	case VIEW::FRONT:
		break;
	case VIEW::BACK:
		mWorldMatrix.rotate(180, 1, 0, 0);
		break;
	case VIEW::RIGHT:
		mWorldMatrix.rotate(-90, 0, 1, 0);
		break;
	case VIEW::LEFT:
		mWorldMatrix.rotate(90, 0, 1, 0);
		break;
	case VIEW::UP:
		mWorldMatrix.rotate(90, 1, 0, 0);
		break;
	case VIEW::DOWN:
		mWorldMatrix.rotate(-90, 0, 1, 0);
		mWorldMatrix.rotate(-90, 1, 0, 0);
		break;
	}
}

QMatrix4x4& Model::worldMatrix()
{
	return mWorldMatrix;
}

void Model::intersectWithRay(Ray &r, int &closest, QList<int> &triList)
{
	QList<float> tauList;
	QList<int> closestList;

	float tau;
	int closestPoinID;
	for (int i = 0; i < mIndices.count() / 3; i++)
	{
		if (intersectTriWithRay(r, i, tau, closestPoinID))
		{
			triList.append(i);
			tauList.append(tau);
			closestList.append(closestPoinID);
		}
	}

	// Sort the List using quick bubblesort
	int n = triList.count();
	if (n > 0)
	{
		do
		{
			int newn = 0;
			for (int i = 1; i < n; i++)
			{
				if (tauList[i - 1] > tauList[i])
				{
					tauList.swap(i - 1, i);
					triList.swap(i - 1, i);
					closestList.swap(i - 1, i);
					newn = i;
				}
			}
			n = newn;
		} while (n != 0);
		closest = closestList[0];
	}
}

bool Model::intersectTriWithRay(Ray &r, int triNum, float &tau, int &closest)
{
	const QVector3D &e = r.start();
	const QVector3D &d = r.dir();

	QVector3D &a = mVertices[mIndices[3 * triNum]];
	QVector3D &b = mVertices[mIndices[3 * triNum + 1]];
	QVector3D &c = mVertices[mIndices[3 * triNum + 2]];

	// Solve the 3x3 equation e + t*d = a + gi*(b - a) + vi*(c - a)
	const float xaxb = a.x() - b.x();
	const float yayb = a.y() - b.y();
	const float zazb = a.z() - b.z();
	const float xaxc = a.x() - c.x();
	const float yayc = a.y() - c.y();
	const float zazc = a.z() - c.z();
	const float xd = d.x();
	const float yd = d.y();
	const float zd = d.z();

	const float D = xaxb * (yayc * zd - yd * zazc)
		- xaxc * (yayb * zd - yd * zazb)
		+ xd * (yayb * zazc - yayc * zazb);
	if (!D)
		return false;

	const float xaxe = a.x() - e.x();
	const float yaye = a.y() - e.y();
	const float zaze = a.z() - e.z();

	const float Dg = xaxb * (yaye * zd - yd * zaze)
		- xaxe * (yayb * zd - yd * zazb)
		+ xd * (yayb * zaze - yaye * zazb);
	const float vi = Dg / D;
	if (vi < 0 || vi > 1)
		return false;

	const float Db = xaxe * (yayc * zd - yd * zazc)
		- xaxc * (yaye * zd - yd * zaze)
		+ xd * (yaye * zazc - yayc * zaze);
	const float gi = Db / D;
	if (gi < 0 || gi > 1 - vi)
		return false;

	// There are three medians described in barycentric coordinates
	// gi = vi, gi = -2 * vi + 1, gi = -0.5 * vi + 0.5
	if (gi < vi)
	{
		if (gi < -2 * vi + 1)
			closest = mIndices[3 * triNum];
		else
			closest = mIndices[3 * triNum + 2];
	}
	else
	{
		if (gi < -0.5 * vi + 0.5)
			closest = mIndices[3 * triNum];
		else
			closest = mIndices[3 * triNum + 1];
	}

	const float Dt = xaxb * (yayc * zaze - yaye * zazc)
		- xaxc * (yayb * zaze - yaye * zazb)
		+ xaxe * (yayb * zazc - yayc * zazb);

	tau = Dt / D;

	return true;
}

void Model::setupVertexAttribs()
{
	// Transfer vertex data to VBO 0
	mVertexBuf.bind();
	mVertexBuf.allocate(mVertices.constData(), mVertices.count() * sizeof(QVector3D));
	mVertexBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
	mVertexBuf.release();

	// Transfer vertex data to VBO 1
	mNormalBuf.bind();
	mNormalBuf.allocate(mNormals.constData(), mNormals.count() * sizeof(QVector3D));
	mNormalBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
	mNormalBuf.release();

	// Transfer vertex data to VBO 2
	mColorBuf.bind();
	mColorBuf.allocate(mColors.constData(), mColors.count() * sizeof(QVector3D));
	mColorBuf.release();

	// Transfer index data to VBO 3
	mIndexBuf.bind();
	mIndexBuf.allocate(mIndices.constData(), mIndices.count() * sizeof(GLuint));
	mIndexBuf.release();
}

void Model::setupBboxVertexAttribs()
{
	// Transfer vertex data to VBO 0
	mBboxBuf.bind();
	mBboxBuf.allocate(mBboxdata.constData(), mBboxdata.count() * sizeof(QVector3D));
	mBboxBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
	mBboxBuf.release();
	//*/

	// Transfer index data to VBO 1
	mBboxindexBuf.bind();
	mBboxindexBuf.allocate(mBboxindices.constData(), mBboxindices.count() * sizeof(GLuint));
	mBboxindexBuf.release();
}

void Model::setupAxesVertexAttribs()
{
	// Transfer vertex data to VBO 0
	mAxesBuf.bind();
	mAxesBuf.allocate(mAxesdata.constData(), mAxesdata.count() * sizeof(QVector3D));
	mAxesBuf.setUsagePattern(QOpenGLBuffer::StaticDraw);
	mAxesBuf.release();
	//*/

	// Transfer index data to VBO 1
	mAxesindexBuf.bind();
	mAxesindexBuf.allocate(mAxesindices.constData(), mAxesindices.count() * sizeof(GLuint));
	mAxesindexBuf.release();
}

BBox& Model::bbox()
{
	return m_bbox;
}

void Model::updateVertices()
{
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);
	mVertexBuf.bind();
	mVertexBuf.write(0, mVertices.constData(), mVertices.count() * sizeof(QVector3D));
	mVertexBuf.release();
	calculateBbox();
	updateBbox();
	emit verticesChanged();
}

void Model::updateNormals()
{
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);
	mNormalBuf.bind();
	mNormalBuf.write(0, mNormals.constData(), mNormals.count() * sizeof(QVector3D));
	mNormalBuf.release();
	emit normalsChanged();
}

void Model::updateColors()
{
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);
	mColorBuf.bind();
	mColorBuf.write(0, mColors.constData(), mColors.count() * sizeof(QVector3D));
	mColorBuf.release();
	emit colorsChanged();
}

void Model::updateBbox()
{
	QOpenGLVertexArrayObject::Binder vaoBboxBinder(&mBboxvao);
	// Update the Bbox
	mBboxBuf.bind();
	mBboxBuf.write(0, mBboxdata.constData(), mBboxdata.count() * sizeof(QVector3D));
	mBboxBuf.release();
	// No need to update bbox indices size it is always the same
}

void Model::updateAll()
{
	QOpenGLVertexArrayObject::Binder vaoBinder(&mVao);
	// Update the Model
	mVertexBuf.bind();
	mVertexBuf.write(0, mVertices.constData(), mVertices.count() * sizeof(QVector3D));
	mVertexBuf.release();

	mNormalBuf.bind();
	mNormalBuf.write(0, mNormals.constData(), mNormals.count() * sizeof(QVector3D));
	mNormalBuf.release();

	mColorBuf.bind();
	mColorBuf.write(0, mColors.constData(), mColors.count() * sizeof(QVector3D));
	mColorBuf.release();

	// allocate the correct size
	mIndexBuf.bind();
	mIndexBuf.allocate(mIndices.constData(), mIndices.count() * sizeof(GLuint));
	mIndexBuf.release();

	QOpenGLVertexArrayObject::Binder vaoBboxBinder(&mBboxvao);
	// Update the Bbox
	mBboxBuf.bind();
	mBboxBuf.write(0, mBboxdata.constData(), mBboxdata.count() * sizeof(QVector3D));
	mBboxBuf.release();
	// No need to update bbox indices size it is always the same

	QOpenGLVertexArrayObject::Binder vaoAxesBinder(&mAxesvao);
	// Update the Axes
	mAxesBuf.bind();
	mAxesBuf.write(0, mAxesdata.constData(), mAxesdata.count() * sizeof(QVector3D));
	mAxesBuf.release();
	// No need to update axes indices size it is always the same
	emit modelChanged();
}
