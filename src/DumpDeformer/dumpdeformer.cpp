#include "dumpdeformer.h"
#include <QtMath>
#include <QDebug>

/*******************************************************************************
Implementation of MeshDeformer methods
******************************************************************************/

DumpDeformer::DumpDeformer(GLWidget* wid) : MeshDeformer(wid)
{
	// Always connect onModelLoaded with modelLoaded in order to initialize
	// the deformer every time a new model is loaded
	connect(openGLWidget, &GLWidget::modelLoaded, this, &DumpDeformer::onModelLoaded);
}

DumpDeformer::~DumpDeformer()
{
	disconnect(openGLWidget, &GLWidget::modelLoaded, this, &DumpDeformer::onModelLoaded);
}

void DumpDeformer::processVert()
{
	for (int i = 0; i < mCustomdataV.count(); i++)
	{
		if (mCustomdataV[i].x() > 0) mCustomdataV[i].setX(mCustomdataV[i].x() + 20);
	}
}

void DumpDeformer::processCols()
{
	qDebug() << mCustomdataC.count() << mCustomdataN.count();
	for (int i = 0; i < mCustomdataC.count(); i++)
	{
		qreal max = qFabs(mCustomdataN[i].x());
		mCustomdataC[i] = QVector3D(1, 0, 0);
		if (qFabs(mCustomdataN[i].y()) > max)
		{
			max = qFabs(mCustomdataN[i].y());
			mCustomdataC[i] = QVector3D(0, 1, 0);
		}
		if (qFabs(mCustomdataN[i].z()) > max)
		{
			mCustomdataC[i] = QVector3D(0, 0, 1);
		}
	}
}

void DumpDeformer::onModelLoaded()
{
	mCustomdataV = openGLWidget->vertices();
	mCustomdataN = openGLWidget->normals();
	mCustomdataC = openGLWidget->colors();
}

void DumpDeformer::onCheckBoxToggled(bool state)
{
	processVert();
	processCols();
	openGLWidget->setVertices(mCustomdataV);
	openGLWidget->setColorMode(GLWidget::COLORMODE::RGB);
	openGLWidget->setColors(mCustomdataC);
	openGLWidget->calculateNormals();
}