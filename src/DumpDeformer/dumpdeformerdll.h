#ifdef WIN32
#   ifdef DumpDeformer_EXPORTS
#       define DumpDeformer_API __declspec(dllexport)
#   else
#       define DumpDeformer_API  __declspec(dllimport)
#   endif
#else
#   define DumpDeformer_API
#endif // WIN32