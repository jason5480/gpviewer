#pragma once

#include "dumpdeformerdll.h"

#include "meshdeformer.h"

/*******************************************************************************
MeshDeformer
******************************************************************************/

class DumpDeformer_API DumpDeformer : public MeshDeformer
{
Q_OBJECT

public:
    // Ctor
    DumpDeformer(GLWidget* wid);
    ~DumpDeformer();

private:
    // Methods
    void processVert();
    void processCols();

private:
    // Data members
    QVector<QVector3D> mCustomdataV;
    QVector<QVector3D> mCustomdataN;
    QVector<QVector3D> mCustomdataC;

public slots:
    void onModelLoaded() Q_DECL_OVERRIDE;
    void onCheckBoxToggled(bool state);
};